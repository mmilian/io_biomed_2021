import socket
import time
from multiprocessing import Pool

HOST_PORT = ("localhost", 8000)

def handle_request(client_connection):
    # print(f"""Wywołanie nr: {counter}""")
    request = client_connection.recv(1024).decode("utf-8")
    print(request)
    time.sleep(1)
    client_connection.sendall(b"""
HTTP/1.1 200 OK

Hello, World!!!
"""
)
    client_connection.close()


sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)  # nieistotne 
sock.bind(HOST_PORT) 
sock.listen(10)
counter = 0
print(f"""Serwer started...""")
with Pool(5) as p:
    while (True):
        counter += 1
        client_connection, client_adress  = sock.accept()
        def close_conn():
            client_connection.close()
        p.apply_async(handle_request, (client_connection,), callback=close_conn)