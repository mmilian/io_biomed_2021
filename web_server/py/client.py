import socket
import os
from time import time

HOST_PORT = ("localhost", 8000)

clients = 10

REQ = b"""GET /hello HTTP/1.1
host: localhost:8000

"""

if os.path.exists("results.csv"): os.remove("results.csv")

counter = 0

for c in range(clients):
    pid = os.fork()
    if pid == 0:
        start = time()
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock_created = time()
        sock.connect(HOST_PORT)
        sock_connected = time()
        sock.sendall(REQ)
        res = sock.recv(4096)
        print(f"""Response size {len(res)}""")
        print(f"""Response: {res.decode("utf-8")}""")
        request_end = time()
        sock.close()
        with open("results.csv", "a+") as f:
            f.write(f"""{c};{sock_created-start};{sock_connected-sock_created};{request_end-sock_created};{request_end-start}\n""")
        print(f"""Request nr {c} and recv data: {res.decode("utf-8")} took {request_end-start}""")
        os._exit(0)
